<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$bbcode = new bbcode();

$query = "select count(*) from community_news";
$result = $mysqli->query($query);
$r = $result->fetch_row();
$numrows = (int) $r[0];

$rowsperpage = $news->display_limit;
$range = $news->display_range;
$totalpages = ceil($numrows / $rowsperpage);

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $currentpage = (int) $_GET['page'];
}else{
    $currentpage = 1;
}

if ($currentpage > $totalpages) {
    $currentpage = $totalpages;
}

if ($currentpage < 1) {
    $currentpage = 1; 
}

$offset = (int) ($currentpage - 1) * $rowsperpage;
$query = "select a.accountId, a.username, a.permission, n.newsId, n.news_header, n.news_message, n.news_date, 
    n.news_author, g.groupId, g.group_name, g.group_color
    from account as a left join community_news as n on a.accountId = n.news_author
    left join system_groups as g on g.groupId = a.permission
    where n.news_author order by n.newsId desc limit $offset, $rowsperpage";

if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $news_row[] = $row;
    }

    $mysqli->close();
}

$date = strtotime(date("Y-m-d"));

$day = date('d', $date);
$month = date('m', $date);
$year = date('Y', $date);

$firstDay = mktime(0, 0, 0, $month, 1, $year);
$title = strftime('%B', $firstDay);
$dayOfWeek = date('D', $firstDay);
$daysInMonth = cal_days_in_month(0, $month, $year);

$timestamp = strtotime('next Sunday');
$weekDays = array();
for ($i = 0; $i < 7; $i++) {
	$weekDays[] = strftime('%a', $timestamp);
	$timestamp = strtotime('+1 day', $timestamp);
}
$blank = date('w', strtotime("{$year}-{$month}-01"));

include(root_tpl_path. 'index_body.php');
