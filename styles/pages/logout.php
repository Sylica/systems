<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$query = $mysqli->query("update account set online = 0 where accountId = '$_SESSION[accId]' limit 1");
mysqli_query($query);

$mysqli->close();

unset($_SESSION['accId']);
session_unset();
session_destroy();

header("Location: index.php");
