<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$cleaner = new cleaner();

$id = (int) $_SESSION['accId'];
if (!is_numeric($id))
    $id = '';

$query = "select a.accountId, b.accountId, b.user_avatar, c.accountId, c.avatar_width, c.avatar_height
    from account as a left join account_data as b on a.accountId = b.accountId 
    left join account_settings as c on a.accountId = c.accountId 
    where a.accountId = '$id'";
$result = $mysqli->query($query);

if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
        $avatar = $row['user_avatar'];
        $avatar_width = $row['avatar_width'];
        $avatar_height = $row['avatar_height'];
    }
}

if (isset($_POST['btr-changeavatar'])) {
    $avatar_width = $cleaner->filter($_POST['avatar_width']);
    $avatar_height = $cleaner->filter($_POST['avatar_height']);
    $newavatar = filter_var($_POST['avatar'], FILTER_SANITIZE_URL);

    $success = true;

    if (!filter_var($newavatar, FILTER_VALIDATE_URL) === true) {
        $urlError = '<div class="errors">Need a valid url for your avatar.</div>';
        $success = false;
    }

    if ($success) {
        $mysqli->query("update account_data set user_avatar = '$newavatar' where accountId = '$id'");
        $mysqli->query("update account_settings set avatar_height = '$avatar_height', avatar_width = '$avatar_width'
            where accountId = '$id'");

        $msg = "<div class='success'>Avatar updated!</div>";
        header("refresh: 2;");
    }
}

if ($avatar != "") {
    $user_avatar = "<img src='$avatar' height='$avatar_height' width='$avatar_width'>";
}

include(root_tpl_path. 'account/usercp_avatar_body.php');
