<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

include(root_tpl_path. 'account/usercp_body.php');
