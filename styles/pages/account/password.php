<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$cleaner = new cleaner();

$id = (int) $_SESSION['accId'];
if (!is_numeric($id))
    $id = '';

$query = "select * from account where accountId = '$id'";
$result = $mysqli->query($query);

if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
        $email = $row['email_address'];
        $password = $row['sha_pass_hash'];
    }
}

if (isset($_POST['btr-changepassword'])) {
    $oldpass = $cleaner->filter(sha1($email .':'. $_POST['oldpass']));
    $newpass = $cleaner->filter(sha1($email .':'. $_POST['newpass']));

    $success = true;

    if (empty($_POST['oldpass']) || empty($_POST['newpass'])) {
        $emptyError .= '<div class="errors">Password fields are empty.</div>';
        $success = false;
    }

    if ($oldpass != $password) {
        $oldError .= '<div class="errors">Current password does not match.</div>';
        $success = false;
    }

    if ($_POST['newpass'] != $_POST['confirmnewpass']) {
        $newError .= '<div class="errors">New passwords do not match.</div>';
        $success = false;
    }

    if ($success) {
        $query = $mysqli->query("update account set sha_pass_hash = '$newpass' where accountId = '$id'");
        $mysqli->query($query);

        $msg .= '<div class="success">Password has been changed!</div>';
        session_unset();
        header("refresh: 2;");
    }

    $mysqli->close();
}

include(root_tpl_path. 'account/usercp_password_body.php');
