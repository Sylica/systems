<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$cleaner = new cleaner();

$id = (int) $_SESSION['accId'];
if (!is_numeric($id))
    $id = '';

$query = "select account.accountId, account_data.user_signature
    from account left join account_data on account.accountId = account_data.accountId 
    where account.accountId = '$id'";
$results = $mysqli->query($query);

if ($results) {
    while ($row = mysqli_fetch_assoc($results)) {
        $signature = $row['user_signature'];
    }
}

if (isset($_POST['btr-signature'])) {
    $signature = $cleaner->filter($_POST['signature']);

    $query = $mysqli->query("update account_data set user_signature = '$signature' where accountId = '$id' limit 1");
    $mysqli->query($query);

    $msg = 'Your signature has been updated!';
    header("refresh: 2;");
}

include(root_tpl_path. 'account/usercp_signature_body.php');
