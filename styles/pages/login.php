<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$cleaner = new cleaner();

if (isset($_POST['btr-login'])) {
    $email = $cleaner->filter($_POST['email']);
    $password = $cleaner->filter($_POST['password']);

    $enc = sha1($email .':'. $password);

    $success = true;

    if (empty($email)) {
        $emailError .= '<div class="errors">Please enter your email address</div>';
        $success = false;
    }

    if (empty($password)) {
        $passError .= '<div class="errors">Please enter a password</div>';
        $success = false;
    }

    $query = $mysqli->query("select * from account 
        where email_address = '$email' and status = 0");
    $status_check = mysqli_fetch_assoc($query);
    if ($status_check) {
        $msg .= '<div class="error">This account is not active</div>';
        $success = false;
    }

    $query = $mysqli->query("select * from account 
        where email_address = '$email' and sha_pass_hash = '$enc' and status = 1");
    $row = $query->fetch_array();
    $count = $query->num_rows;

    if ($success) {
        if ($query && $count == 1) {
            $_SESSION['logged_in'] = true;
            $_SESSION['username'] = $row['username'];

            $_SESSION['accId'] = $row['accountId'];
            if ($row['permission'] == 3) {
                $_SESSION['admin'] = true;
            }

            $last_login = date("Y-m-d H:i:s");
            $query = $mysqli->query("update account set last_login = '$last_login' where accountId = '$_SESSION[accId]' limit 1");
            $query = $mysqli->query("update account set online = 1 where accountId = '$_SESSION[accId]' limit 1");
            mysqli_query($query);
            
            header("Location: index.php");
        }else{
            $msg = 'Incorrect Credentials, Try again...';
        }
    }

    $mysqli->close();
}

include(root_tpl_path. 'login_body.php');
