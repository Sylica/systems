<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$cleaner = new cleaner();

$query = "select zone_id, timezone_strings from system_countries";
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $timezone[] = $row;
    }
}

if (isset($_POST['btr-application'])) {
    $username = $cleaner->filter($_POST['username']);
    $password = $cleaner->filter($_POST['password']);
    $cpassword = $cleaner->filter($_POST['cpassword']);
    $email = $cleaner->filter($_POST['email']);
    $cemail = $cleaner->filter($_POST['cemail']);
    $referred = $cleaner->filter($_POST['referred']);
    $applied_as = $cleaner->filter($_POST['applied_as']);
    $about_yourself = $cleaner->filter($_POST['about_yourself']);
    $user_experience = $cleaner->filter($_POST['user_experience']);
    $interested = $cleaner->filter($_POST['user_interested']);
    $timezone = $cleaner->filter($_POST['user_timezone']);

    $date = date("Y-m-d H:i:s");
    $enc = sha1($email .':'. $password);

    $ip = $_SERVER['REMOTE_ADDR'];

    $success = true; 

    if (empty($username)) {
        $userError .= '<div class="errors">Please choose a username</div>';
        $success = false;
    }

    if (!preg_match("/^[a-zA-Z]+$/", $username)) {
        $matchError .= '<div class="errors">Invalid alphabetical for username</div>';
        $success = false;
    }

    if (empty($password)) {
        $passError .= '<div class="errors">Please choose a password</div>';
        $success = false;
    }

    if (empty($cpassword)) {
        $cpassError .= '<div class="errors">Repeat password is empty</div>';
        $success = false;
    }

    if ($password != $cpassword) {
        $matchPass .= '<div class="errors">Passwords do not match</div>';
        $success = false;
    }

    if (empty($email)) {
        $emailError .= '<div class="errors">Please choose an email address</div>';
        $success = false;
    }

    if (empty($cemail)) {
        $cemailError .= '<div class="errors">Repeat email address is empty</div>';
        $success = false;
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $validError .= '<div class="errors">Please use a real email address</div>';
        $success = false;
    }

    if (empty($applied_as) != "") {
        $applyError .= '<div class="errors">What are you applying for</div>';
        $success = false;
    }

    if (empty($timezone) != "") {
        $timeError .= '<div class="errors">What timezone are you in</div>';
        $success = false;
    }

    if (empty($interested) != "") {
        $interestedError .= '<div class="errors">Tell us why you like to join our community</div>';
        $success = false;
    }

    $query = $mysqli->query("select username from account where username = '$username'");
    $user_check = mysqli_fetch_assoc($query);
    if ($user_check) {
        $userUsed .= '<div class="errors">Username already registered</div>';
        $success = false;
    }

    $query = $mysqli->query("select email_address from account where email_address = '$email'");
    $email_check = mysqli_fetch_assoc($query);
    if ($email_check) {
        $emailUsed .= '<div class="errors">Email address already registered</div>';
        $success = false;
    }

    if ($success) {
        $mysqli->query("insert into account (username, sha_pass_hash, email_address, remote_address, joined_date)
            values ('$username', '$enc', '$email', '$ip', '$date')");

        $mysqli->query("insert into account_data (user_timezone) values ('$timezone')");

        $mysqli->query("insert into account_settings (avatar_height, avatar_width)
            values ('0', '0')");

        $mysqli->query("insert into community_application (application_applied_as, application_referred, application_about, application_experience, application_interested)
            values ('$applied_as', '$referred', '$about_yourself', '$user_experience', '$interested')");

        $alert .= '<div class="success">Application Submitted!<br/>Account will need approval before login.</div>';
    }

    $mysqli->close();
}

include(root_tpl_path. 'recruitment_body.php');
