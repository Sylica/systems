<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$bbcode = new bbcode();
$account = new account();

$id = (int) $_GET['u'];
if (!is_numeric($id))
    $id = '';

$query = "select accountId from account where accountId = '$id'";
$result = $mysqli->query($query);
if (mysqli_num_rows($result) != 1) {
    header("Location: index.php?action=404");
}

$query = "select a.accountId, a.username, a.email_address, a.last_login, a.online, 
    a.joined_date, a.permission, c.accountId, c.avatar_height, c.avatar_width, 
    p.user_signature, p.user_avatar, s.zone_id, s.timezone_strings, g.groupId, g.group_name, g.group_color
    from account as a left join account_data as p on a.accountId = p.accountId 
    left join account_settings as c on a.accountId = c.accountId
    left join system_countries as s on s.zone_id = p.user_timezone
    left join system_groups as g on g.groupId = a.permission
    where a.accountId = '$id'";

if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $ud1 = $row;
    }

    $mysqli->close();
}

include(root_tpl_path. 'member/memberlist_viewprofile_body.php');
