<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$query = "select count(*) from account";
$result = $mysqli->query($query);
$r = $result->fetch_row();
$numrows = (int) $r[0];

$rowsperpage = 25;
$range = 5;
$totalpages = ceil($numrows / $rowsperpage);

if (isset($_POST['page']) && is_numeric($_POST['page'])) {
    $currentpage = (int) $_POST['page'];
}else{
    $currentpage = 1;
}

if ($currentpage > $totalpages) {
    $currentpage = $totalpages;
}

if ($currentpage < 1) {
    $currentpage = 1;
} 

$offset = (int) ($currentpage - 1) * $rowsperpage;
$query = "select a.accountId, a.username, a.permission, a.joined_date, a.status, b.accountId, 
    b.user_avatar, g.groupId, g.group_name, g.group_color
    from account as a left join account_data as b on a.accountId = b.accountId
    left join system_groups as g on g.groupId = a.permission
    where a.status in (1, 2) order by a.username asc limit $offset, $rowsperpage";

if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $member_row[] = $row;
    }

    $mysqli->close();
}

include(root_tpl_path. 'member/memberlist_body.php');
