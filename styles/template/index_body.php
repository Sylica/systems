            <div id="mainpanel"> 
                <div class="rightbox">
                    <div class="calendar">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Community Calendar</div>
                                </div>

                                <table>
                                    <tr>
                                        <th colspan="7"><?php print $title ?> <?php print $year ?></th>
                                    </tr>
                                    <tr><?php foreach ($weekDays as $key => $weekDay): ?> 
                                        <td><?php print $weekDay ?></td>
                                    <?php endforeach ?></tr>

                                    <tr><?php for ($i = 0; $i < $blank; $i++): ?> 
                                        <td></td>
                                        <?php endfor; ?>
                                        <?php for ($i = 1; $i <= $daysInMonth; $i++): ?>
                                            <?php if ($day == $i): ?> 
                                        <td><strong><?php print $i ?></strong></td>
                                            <?php else: ?> 
                                        <td><?php print $i ?></td>
                                            <?php endif; ?>
                                            <?php if (($i + $blank) % 7 == 0): ?> 
                                        </tr><tr>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                        <?php for ($i = 0; ($i + $blank + $daysInMonth) % 7 != 0; $i++): ?> 
                                        <td></td>
                                    <?php endfor; ?></tr>
                                </table> 
                            </div>
                        </div>
                    </div>

                    <div class="discord">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Community Discord <span>+ Join Us</span></div>
                                </div>
                                <p>
                                    <iframe src="https://discordapp.com/widget?id=287362927474901002&theme=dark" 
                                        width="280" height="280" allowtransparency="false" frameborder="0"></iframe>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contentbox">
                    <div class="articlepost">
                        <?php foreach ($news_row as $news): ?> 
                        <div class="articlebox">
                            <div class="block">
                                <div class="block-bot">
                                    <div class="head">
                                        <div class="head-cnt"><?php print $news['news_header']; ?> <span>Category: General</span></div>
                                    </div>

                                    <div class="message">
                                        <?php print $bbcode->bbcode_to_html($news['news_message']); ?> 
                                    </div>
                                    
                                    <div class="clear"></div>
                                    
                                    <div class="details">Authored by <font color="<?php print $news['group_color']; ?>"><?php print ucfirst($news['username']); ?></font> on <?php print date('F jS, Y', strtotime($news['news_date'])); ?></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endforeach; ?>             
                    </div>

                    <div class="pagination">
                    <?php if ($currentpage > 1) {
                            print " <a href='index.php?page=1'><<</a> ";

                            $prevpage = $currentpage - 1;
                            print " <a href='index.php?page=$prevpage'><</a> ";
                        }

                        for ($i = ($currentpage - $range); $i < (($currentpage + $range) + 1); $i++) {
                            if (($i > 0) && ($i <= $totalpages)) {
                                if ($i == $currentpage) {
                                    print "[<b>$i</b>]";
                                }else{
                                    print " <a href='index.php?page=$i'> $i </a> ";
                                }
                            } 
                        }

                        if ($currentpage != $totalpages) {
                            $nextpage = $currentpage + 1;
                            print " <a href='index.php?page=$nextpage'>></a> ";
                            print " <a href='index.php?page=$totalpages'>>></a> ";
                        } ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>
