        
        <div id="main-nav">
            <div class="bg-right">
                <div class="bg-left">
                    <ul><?php
                            if (isset($_SESSION['admin'])): ?> 
                        <li><a href="<?php print $website->url; ?>admin/" target="_blank">Admin CP</a></li><?php
                        endif; ?> 
                        <li><a href="<?php print $website->url; ?>forum/">Forum</a></li><?php 
                            if (isset($_SESSION['logged_in'])): ?> 
                        <li><a href="<?php print $website->url; ?>member.php">Member List</a></li>
                        <li><a href="<?php print $website->url; ?>index.php?action=downloads">Downloads</a></li><?php 
                        endif; ?> 
                        <li><a href="#">About Us</a></li>
                        <li><a href="<?php print $website->url; ?>index.php?action=contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="clear"></div>
        
        <div id="sort-nav">
            <div class="bg-right">
                <div class="bg-left">
                    <ul>
                        <li>&nbsp;</li>
                        <li><a href="<?php print $website->url; ?>index.php">Home Page</a></li><?php 
                         if (isset($_SESSION['logged_in'])): ?> 
                        <div class="dropdown">
                            <button onclick="dropdownMenu()" class="dropdownbtn">User Panel</button>
                            <div id="dropdownMenu" class="dropdown-content">
                                <a href="<?php print $website->url; ?>account.php">User Control Panel</a>
                                <a href="<?php print $website->url; ?>member.php?action=viewprofile&u=<?php print $_SESSION['accId']; ?>">View My Profile</a>
                            </div>
                        </div>
                        <li><a href="<?php print $website->url; ?>index.php?action=logout">Logout</a></li>    
                        <?php else: ?> 
                        <li><a href="<?php print $website->url; ?>index.php?action=login">Login</a></li>
                        <li><a href="<?php print $website->url; ?>index.php?action=recruitment">Recruitment</a></li>  
                        <?php endif; ?>  
                    </ul>
                </div>
            </div>
        </div>
