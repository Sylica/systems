            <div id="application">
                <div class="block">
                    <div class="block-bot">
                        <div class="head">
                            <div class="head-cnt">Membership Application</div>
                        </div>
                        <?php print $alert; ?> 
                        <form method="post" autocomplete="off">
                            <div class="left">
                                <label><font color="#ff0000">*</font> Username</label>
                                <input type="text" name="username" maxlength="15" value="<?php 
                                    if (isset($_POST['username'])) print $_POST['username']; ?>">
                                <?php
                                    if (!$username) { print $userError; } else if 
                                        ($user_check) { print $userUsed; } else { print $matchError; }
                                ?>

                                <label><font color="#ff0000">*</font> Email Address</label>
                                <input type="text" name="email" value="<?php 
                                    if (isset($_POST['email'])) print $_POST['email']; ?>">
                                <?php
                                    if (!$email) { print $emailError; } else if 
                                        ($validError) { print $validError; } else { print $emailUsed; } 
                                ?> 

                                <label><font color="#ff0000">*</font> Password</label>
                                <input type="password" name="password" minlength="8">
                                <?php if (!$password) { print $passError; } else { print $matchPass; } ?> 

                                <label>If referred by a member, who</label>
                                <input type="text" name="referred" value="<?php 
                                    if (isset($_POST['referred'])) print $_POST['referred']; ?>">
                            </div>

                            <div class="right">
                                <label><font color="#ff0000">*</font> I'm applying as a(n)</label>
                                <select name="applied_as">
                                    <option value=""></option>
                                    <option value="1">New Member</option>
                                    <option value="2">Existing Member</option>
                                    <option value="3">Existing Officer</option>
                                </select>
                                <?php print $applyError; ?> 

                                <label><font color="#ff0000">*</font> Repeat Email Address</label>
                                <input type="text" name="cemail" value="<?php 
                                    if (isset($_POST['cemail'])) print $_POST['cemail']; ?>">
                                <?php print $cemailError; ?>

                                <label><font color="#ff0000">*</font> Repeat Password</label>
                                <input type="password" name="cpassword" minlength="8">
                                <?php print $cpassError; ?>

                                <label><font color="#ff0000">*</font> Your Timezone</label>
                                <select name="user_timezone">
                                    <?php foreach ($timezone as $tz) {
                                            print "<option value='". $tz['zone_id'] ."'>". $tz['timezone_strings'] ."</option>";
                                        } ?>
                                </select>
                                <?php print $timeError; ?>
                            </div>

                            <div class="clear"><br/></div>

                            <div class="bottom">
                                <label>Please tell us a little about yourself</label>
                                <textarea class="textarea" name="about_yourself"><?php 
                                    if (isset($_POST['about_yourself'])) print $_POST['about_yourself']; ?></textarea>

                                <label>Do you have leadership experience</label>
                                <textarea class="textarea" name="user_experience"><?php 
                                    if (isset($_POST['user_experience'])) print $_POST['user_experience']; ?></textarea>

                                <label><font color="#ff0000">*</font> Please explain why you are interested in our community</label>
                                <textarea class="textarea" name="user_interested"><?php 
                                    if (isset($_POST['user_interested'])) print $_POST['user_interested']; ?></textarea>
                                <?php print $interestedError; ?>
                            </div>

                            <input class="submit" name="btr-application" type="submit" value="Submit Application">
                        </form>
                    </div>
                </div>
            </div>
