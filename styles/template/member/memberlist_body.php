            <div id="memberpanel"> 
                <div class="contentbox">
                    <div class="memberlist">
                        <div class="memberbox">
                            <div class="block">
                                <div class="block-bot">
                                    <div class="head">
                                        <div class="head-cnt">Members List 
                                            <span>
                                                <form method="post">
                                                    <select>
                                                        <option value="">View All</option>
                                                    </select>
    
                                                    <input class="submit" type="submit" value="Filter Records">
                                                </form>
                                            </span>
                                        </div>
                                    </div>
                                    <table>
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td width="70">Username</td>
                                            <td width="100">Rank</td>
                                            <td width="70">Posts</td>
                                            <td width="200">Location</td>
                                            <td width="100">Joined</td>
                                        </tr>
                                        <?php foreach ($member_row as $user): ?>
                                        <tr>
                                            <td width="10"><?php if ($user['user_avatar'] != '') { ?><img src="<?php print $user['user_avatar']; ?>" width="30" height="30"><?php } ?></td>
                                            <td width="70"><a href="member.php?action=viewprofile&u=<?php print $user['accountId']; ?>"><?php print ucfirst($user['username']); ?></a></td>
                                            <td width="100"><font color="<?php print $user['group_color']; ?>"><?php print $user['group_name']; ?></td>
                                            <td width="70">---</td>
                                            <td width="200">---</td>
                                            <td width="100"><?php print date('M jS, Y', strtotime($user['joined_date'])); ?></td>
                                        </tr>
                                        <?php endforeach; ?> 
                                    </table>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>             
                    </div>

                    <div class="pagination">
                    <?php if ($currentpage > 1) {
                            print " <a href='member.php?page=1'><<</a> ";

                            $prevpage = $currentpage - 1;
                            print " <a href='member.php?page=$prevpage'><</a> ";
                        }

                        for ($i = ($currentpage - $range); $i < (($currentpage + $range) + 1); $i++) {
                            if (($i > 0) && ($i <= $totalpages)) {
                                if ($i == $currentpage) {
                                    print "[<b>$i</b>]";
                                }else{
                                    print " <a href='member.php?page=$i'> $i </a> ";
                                }
                            }
                        }

                        if ($currentpage != $totalpages) {
                            $nextpage = $currentpage + 1;
                            print " <a href='member.php?page=$nextpage'>></a> ";
                            print " <a href='member.php?page=$totalpages'>>></a> ";
                        } ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>
