            <div id="userprofile">
                <div class="leftbox">
                    <div class="userdetails">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt"><?php print ucfirst($ud1['username']); ?></div>
                                </div>  
                                <div class="avatar">
                                    <?php if ($ud1['user_avatar'] != "") {
                                        print '<img src="'.htmlentities($ud1['user_avatar']).'" width="'.htmlentities($ud1['avatar_width']).'" height="'.htmlentities($ud1['avatar_height']).'" />';
                                    } ?> 
                                </div>
                                <ul>
                                    <li>Group <span><font color="<?php print $ud1['group_color']; ?>"><?php print $ud1['group_name']; ?></font></span></li>                
                                    <li>Status <span><?php print $account->status($ud1['online']); ?></span></li>
                                    <li>Join Date <span><?php print date('M jS, y', strtotime($ud1['joined_date'])); ?></span></li>
                                    <li>Last Log In <span><?php 
                                        if ($ud1['last_login'] != "") {
                                            print date('M jS, y', strtotime($ud1['last_login']));
                                        }else{
                                            print "---";
                                        }
                                     ?></span></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>     

                <div class="contentbox">                  
                    <div class="tab">
                        <button class="tablinks" onclick="openTab(event, 'posts')">Forum Posts</button>
                        <?php if ($ud1['user_signature'] != ""): ?><button class="tablinks" onclick="openTab(event, 'signature')">Signature</button><?php endif; ?> 
                        <button class="tablinks" onclick="openTab(event, 'livestream')">Livestream</button>
                    </div>

                    <div id="posts" class="tabcontent" style="display: block"> 
                        <div class="message">
                            Forum posts is under construction at this time
                        </div>
                    </div>
                    <?php if ($ud1['user_signature'] != "") { ?> 
                    <div id="signature" class="tabcontent">
                        <div class="message">
                            <?php print $bbcode->bbcode_signature($ud1['user_signature']); ?> 
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php } ?> 

                    <div id="livestream" class="tabcontent">
                        <div class="message">
                            Livestream is under construction at this time
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>
