            <div id="login">
                <div class="block">
                    <div class="block-bot">
                        <div class="head">
                            <div class="head-cnt">Community Login</div>
                        </div>
                        <?php if ($msg) { ?> 
                        <div class="error"><?php print $msg; ?></div>
                        <?php } ?> 
                        <form method="post" autocomplete="off">
                            <div class="body">
                                <label>Email Address</label>
                                <input type="text" name="email">
                                <?php print $emailError; ?>

                                <label>Password</label>
                                <input type="password" name="password">
                                <?php print $passError; ?>

                                <input class="submit" name="btr-login" type="submit" value="Login">
                            </div>
                        </form>
                        <div class="space"></div>
                    </div>
                </div>
            </div>
