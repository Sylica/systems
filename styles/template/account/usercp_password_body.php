            <div id="userpanel">
                <div class="leftbox">
                    <div class="usermenu">
                        <?php require_once(root_tpl_path. 'account/misc/usercp_leftside_box.php'); ?> 
                    </div>
                </div>

                <div class="contentbox">
                    <div class="mainpanel">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Change Password</div>
                                </div>
                                <?php print $msg; ?> 
                                <form method="post" autocomplete="off">
                                    <div class="body">
                                        <label>Old Password</label>
                                        <input type="password" name="oldpass">
                                        <?php print $oldError; ?>

                                        <label>New Password</label>
                                        <input type="password" name="newpass" minlength="8">
                                        <?php print $emptyError; ?>

                                        <label>Repeat New Password</label>
                                        <input type="password" name="confirmnewpass">

                                        <input class="submit" type="submit" name="btr-changepassword" value="Change Password">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
