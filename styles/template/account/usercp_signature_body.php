            <div id="userpanel">
                <div class="leftbox">
                    <div class="usermenu">
                        <?php require_once(root_tpl_path. 'account/misc/usercp_leftside_box.php'); ?> 
                    </div>
                </div>

                <div class="contentbox">
                    <div class="mainpanel">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Edit Signature</div>
                                </div>
                                <div class="success"><?php print $msg; ?></div>
                                <form method="post">
                                    <div class="signature_body" autocomplete="off">
                                        <div class="message_buttons">
                                            <input type="button" value="B" onclick="javascript:insert('[b]', '[/b]', 'message');" /><!--
                                            --><input type="button" value="I" onclick="javascript:insert('[i]', '[/i]', 'message');" /><!--
                                            --><input type="button" value="U" onclick="javascript:insert('[u]', '[/u]', 'message');" /><!--
                                            --><input type="button" value="Image" onclick="javascript:insert('[img=w,50]', '[/img]', 'message');" /><!--
                                            --><input type="button" value="Image Left" onclick="javascript:insert('[imgleft=w,50]', '[/imgleft]', 'message');" /><!--
                                            --><input type="button" value="Link" onclick="javascript:insert('[url]', '[/url]', 'message');" /><!--
                                            --><input type="button" value="Left" onclick="javascript:insert('[left]', '[/left]', 'message');" /><!--
                                            --><input type="button" value="Center" onclick="javascript:insert('[center]', '[/center]', 'message');" /><!--
                                            --><input type="button" value="Right" onclick="javascript:insert('[right]', '[/right]', 'message');" />
                                        </div>
                                        <textarea class="textarea" id="message" name="signature"><?php print $signature; ?></textarea>

                                        <input class="submit" name="btr-signature" type="submit" value="Edit Signature">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
