            <div id="userpanel">
                <div class="leftbox">
                    <div class="usermenu">
                        <?php require_once(root_tpl_path. 'account/misc/usercp_leftside_box.php'); ?> 
                    </div>
                </div>

                <div class="contentbox">
                    <div class="mainpanel">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Change Avatar</div>
                                </div>
                            </div>
                           
                            <div class="avatar">
                                <?php print $user_avatar; ?>
                            </div>
                            <?php print $msg; ?> 

                            <form method="post"> 
                                <div class="body">
                                    <label>Avatar Size <small>(height x width)</small></label>
                                    <input type="text" class="avatar_height" name="avatar_height" value="<?php print $avatar_height; ?>"> 
                                    <input type="text" class="avatar_width" name="avatar_width" value="<?php print $avatar_width; ?>">

                                    <label>Remote Avatar</label>
                                    <input type="text" name="avatar" placeholder="http://domain.com/your-avatar-image.jpg">
                                    <?php print $urlError; ?>
                                    
                                    <input class="submit" type="submit" name="btr-changeavatar" value="Change Avatar">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
