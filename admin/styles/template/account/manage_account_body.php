            <div id="adminpanel">
                <div class="leftbox">
                    <?php include(admin_tpl_path. 'misc/account_side_menu.php'); ?>
                </div>

                <div class="globalpanel">
                    <div class="contentbox">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">
                                        Account Overview
                                        <span>
                                            <form method="post">
                                                <select name="value">
                                                    <option value=""></option>
                                                    <option value="0">Waiting Approval</option>
                                                    <option value="1">Approved</option>
                                                    <option value="2">LoA / MIA</option>
                                                    <option value="3">Disabled</option>
                                                    <option value="4">Banned</option>
                                                </select>
 
                                                <input class="submit" type="submit" value="Filter Records">
                                            </form>
                                        </span>
                                    </div>
                                </div>
                                <table>
                                    <?php while ($list = mysqli_fetch_assoc($result)) { ?> 
                                    <tr>
                                        <td width="150"><?php print ucfirst($list['username']); ?></td>
                                        <td width="300"><?php print $list['email_address']; ?></td>
                                        <td width="200"><?php print date('l M jS, Y', strtotime($list['joined_date'])); ?></td>
                                        <td width="150"><?php print ($list['status']); ?></td>
                                        <td width="110"><a href="#"><span>View Account</span></a></td>
                                    </tr>
                                    <?php } ?> 
                                </table>
                            </div>
                        </div>                 
                    </div>
                    <div class="pagination">
                        <?php
                            if ($currentpage > 1) {
                                print " <a href='index.php?admin=accounts&page=1'><<</a> ";

                                $prevpage = $currentpage - 1;
                                print " <a href='index.php?admin=accounts&page=$prevpage'><</a> ";
                            }

                            for ($i = ($currentpage - $range); $i < (($currentpage + $range) + 1); $i++) {
                                if (($i > 0) && ($i <= $totalpages)) {
                                    if ($i == $currentpage) {
                                        print "[<b>$i</b>]";
                                    }else{
                                        print " <a href='index.php?admin=accounts&page=$i'> $i </a> ";
                                    }
                                }
                            }

                            if ($currentpage != $totalpages) {
                                $nextpage = $currentpage + 1;
                                print " <a href='index.php?admin=accounts&page=$nextpage'>></a> ";
                                print " <a href='index.php?admin=accounts&page=$totalpages'>>></a> ";
                            }
                        ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>
