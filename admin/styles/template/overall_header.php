<?php global $website, $news; ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php print $website->title; ?></title>
        <link rel="stylesheet" href="<?php print $website->main_theme; ?>" />
        <script type="text/javascript" src="styles/js/javascript.js"></script>
    </head>
<body>
    <div id="wrapper">

        <div id="header">
            <h1><?php print $website->title; ?></h1>
        </div>

        <div class="clear"></div>
        
        <div id="sort-nav">
            <div class="bg-right">
                <div class="bg-left">
                    <ul>
                        <li>&nbsp;</li>
                        <li><a href="index.php">Main Index</a></li>
                        <li><a href="accounts.php">Accounts</a></li>
                        <li><a href="news.php">News</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div id="content">
