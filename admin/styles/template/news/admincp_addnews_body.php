            <div id="adminpanel">
                <div class="leftbox">
                    <?php include(admin_tpl_path. 'misc/news_side_menu.php'); ?>
                </div>

                <div class="globalpanel">
                    <div class="contentbox">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Create News Post</div>
                                </div>
                                <?php print $alert; ?>
                                <div id="newsform">
                                    <div class="formbody">
                                        <form method="post" autocomplete="off">
                                            <label>News Title</label>
                                            <input type="text" name="news_title" maxlength="50">
                                            <?php if ($news_title) {
                                                print $alphaError;
                                            }else{
                                                print $titleError;
                                            } ?>

                                            <div class="message_buttons">
                                                <input type="button" value="Bold" onclick="javascript:insert('[b]', '[/b]', 'message');" /><!--
                                                --><input type="button" value="Italic" onclick="javascript:insert('[i]', '[/i]', 'message');" /><!--
                                                --><input type="button" value="Underlined" onclick="javascript:insert('[u]', '[/u]', 'message');" /><!--
                                                --><input type="button" value="Image" onclick="javascript:insert('[img]', '[/img]', 'message');" /><!--
                                                --><input type="button" value="Link" onclick="javascript:insert('[url]', '[/url]', 'message');" /><!--
                                                --><input type="button" value="Left" onclick="javascript:insert('[left]', '[/left]', 'message');" /><!--
                                                --><input type="button" value="Center" onclick="javascript:insert('[center]', '[/center]', 'message');" /><!--
                                                --><input type="button" value="Right" onclick="javascript:insert('[right]', '[/right]', 'message');" />
                                            </div>
                                            <textarea class="textarea" id="message" name="news_body" minlength="20"></textarea>
                                            <?php print $messageError; ?>
                                            
                                            <input class="submit" name="btr-addnews" type="submit" value="Create News">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>                   
                    </div>
                </div>
                <div class="clear"></div>
            </div>
