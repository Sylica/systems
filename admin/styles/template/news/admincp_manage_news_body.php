            <div id="adminpanel">
                <div class="leftbox">
                    <?php include(admin_tpl_path. 'misc/news_side_menu.php'); ?>
                </div>

                <div class="globalpanel">
                    <div class="contentbox">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Manage News Posts</div>
                                </div>
                                <table>
                                    <tr>
                                        <td width="300">News Title</td>
                                        <td width="100">News Author</td>
                                        <td width="300">News Date</td>
                                        <td width="100">&nbsp;</td>
                                    </tr>
                                    <?php while ($list = mysqli_fetch_assoc($result)) { ?> 
                                    <tr>
                                        <td width="300"><?php print $list['news_header']; ?></td>
                                        <td width="100"><?php print ucfirst($list['username']); ?></td>
                                        <td width="300"><?php  print date('F jS, Y', strtotime($list['news_date'])); ?></td>
                                        <td width="100"><a href="news.php?deletenews=<?php print $list['newsId']; ?>"><span>Delete Post</span></a></td>
                                    </tr>
                                    <?php } ?> 
                                </table>
                            </div>
                        </div>                 
                    </div>
                    <div class="pagination">
                        <?php
                            if ($currentpage > 1) {
                                print " <a href='index.php?admin=news&page=1'><<</a> ";

                                $prevpage = $currentpage - 1;
                                print " <a href='index.php?admin=news&page=$prevpage'><</a> ";
                            }

                            for ($i = ($currentpage - $range); $i < (($currentpage + $range) + 1); $i++) {
                                if (($i > 0) && ($i <= $totalpages)) {
                                    if ($i == $currentpage) {
                                        print "[<b>$i</b>]";
                                    }else{
                                        print " <a href='index.php?admin=news&page=$i'> $i </a> ";
                                    }
                                }
                            }

                            if ($currentpage != $totalpages) {
                                $nextpage = $currentpage + 1;
                                print " <a href='index.php?admin=news&page=$nextpage'>></a> ";
                                print " <a href='index.php?admin=news&page=$totalpages'>>></a> ";
                            }
                        ?> 
                    </div>
                </div>
                <div class="clear"></div>
            </div>