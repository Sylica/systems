            <div id="footer">
                <div class="navs">
                    <div class="navs-bot">
                        <ul>
                            <li>&nbsp;</li>
                        </ul>
                    </div>
                </div>
                <div class="copy">
                    Powered by Guild Management &copy; 2008 - <?php print date("Y"); ?> <br/>
                    <?php print $website->title; ?> &copy; <?php print date("Y"); ?> &bull; All rights reserved!
                </div>
            </div>
        </div>
    </div>
</body>
</html>
