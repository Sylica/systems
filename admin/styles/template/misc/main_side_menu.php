                    
                    <div class="adminmenu">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">Main Site Settings</div>
                                </div>
                                <ul>
                                    <li>&bull; <a href="#">Edit Site Settings</a></li>
                                    <li>&bull; <a href="#">Edit News Post Settings</a></li>
                                    <li>&bull; <a href="#">Edit Forum Post Settings</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="adminmenu">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">User Account Settings</div>
                                </div>
                                <ul>
                                    <li>&bull; <a href="#">Edit Avatar Settings</a></li>
                                    <li>&bull; <a href="#">Edit Signature Settings</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="adminmenu">
                        <div class="block">
                            <div class="block-bot">
                                <div class="head">
                                    <div class="head-cnt">System Logs</div>
                                </div>
                                <ul>
                                    <li>&bull; <a href="#">View Admin Logs</a></li>
                                    <li>&bull; <a href="#">View Login Logs</a></li>
                                    <li>&bull; <a href="#">View Moderation Logs</a></li>
                                    <li>&bull; <a href="#">View Systems Logs</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
