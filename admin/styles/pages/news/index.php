<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

/* Fetch Pages for Community News */
$query = "select count(*) from community_news";
$result = $mysqli->query($query);
$r = mysqli_fetch_row($result);
$numrows = $r[0];

$rowsperpage = 25;
$range = 5;
$totalpages = ceil($numrows / $rowsperpage);

$ip = $_SERVER['REMOTE_ADDR'];
$date = date("Y-m-d H:i:s");


if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $currentpage = (int) $_GET['page'];
}else{
    $currentpage = 1;
}

if ($currentpage > $totalpages) {
    $currentpage = $totalpages;
}

if ($currentpage < 1) {
    $currentpage = 1;
}

$offset = ($currentpage - 1) * $rowsperpage;
$query = "select a.accountId, a.username, n.newsId, n.news_header, n.news_message, n.news_date, n.news_author 
    from account as a left join community_news as n on a.accountId = n.news_author
    where n.news_author order by n.newsId desc limit $offset, $rowsperpage";
$result = $mysqli->query($query);

if (isset($_GET['deletenews'])) {
    if ($_GET['deletenews'] != null) {
        $id = $_GET['deletenews'];
        $query = $mysqli->query("delete from community_news where newsId = '$id' limit 1");
        $mysqli->query("insert into system_logs (accountId, logged_action, remote_host, logged_date)
        values ('$_SESSION[user]', 'Deleted news post', '$ip', '$date')");
        header("Location: news.php");
    }
}

include(admin_tpl_path. 'news/admincp_manage_news_body.php');
