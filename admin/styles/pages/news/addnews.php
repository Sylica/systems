<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$cleaner = new cleaner();

if (isset($_POST['btr-addnews'])) {
    $news_title = $cleaner->filter($_POST['news_title']);
    $news_message = $cleaner->filter($_POST['news_body']);

    $ip = $_SERVER['REMOTE_ADDR'];
    $date = date("Y-m-d H:i:s");

    $success = true;

    if (empty($news_title)) {
        $titleError .= '<div class="errors">News title can not be empty.</div>';
        $success = false;
    }

    if (!preg_match("/^[a-zA-Z ]+$/", $news_title)) {
        $alphaError .= '<div class="errors">Only alphabet characters are allowed</div>';
        $success = false;
    }

    if (empty($news_message)) {
        $messageError .= '<div class="errors">Message field can not be empty. Min length is 20 characters.</div>';
        $success = false;
    }

    if ($success) {
        $mysqli->query("insert into community_news (news_header, news_message, news_date, news_author)
        values ('$news_title', '$news_message', '$date', '$_SESSION[accId]')");

        $mysqli->query("insert into system_logs (accountId, logged_action, remote_host, logged_date)
        values ('$_SESSION[accId]', 'Added news post', '$ip', '$date')");
        
        $alert .= '<div class="success">News successfully posted.</div>';
        header("refresh: 2;");
    }
}

include(admin_tpl_path. 'news/admincp_addnews_body.php');
