<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

if (!isset($_SESSION['admin']))
    header("Location: ../index.php");

include(admin_tpl_path. 'index_body.php');
