<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$account = new account();

/* Fetch Pages for Accounts */
$query = "select count(*) from account";
$result = $mysqli->query($query);
$r = mysqli_fetch_row($result);
$numrows = $r[0];

$rowsperpage = 25;
$range = 5;
$totalpages = ceil($numrows / $rowsperpage);

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $currentpage = (int) $_GET['page'];
}else{
    $currentpage = 1;
}

if ($currentpage > $totalpages) {
    $currentpage = $totalpages;
}

if ($currentpage < 1) {
    $currentpage = 1;
}

$offset = ($currentpage - 1) * $rowsperpage;

if ($_POST['value'] == "0") {
    $sql = "select * from account where status = 0 order by accountId asc limit $offset, $rowsperpage"; 
}elseif ($_POST['value'] == "1") {
    $sql = "select * from account where status = 1 order by accountId asc limit $offset, $rowsperpage";
}elseif ($_POST['value'] == "2") {
    $sql = "select * from account where status = 2 order by accountId asc limit $offset, $rowsperpage";
}elseif ($_POST['value'] == "3") {
    $sql = "select * from account where status = 3 order by accountId asc limit $offset, $rowsperpage";
}elseif ($_POST['value'] == "4") {
    $sql = "select * from account where status = 4 order by accountId asc limit $offset, $rowsperpage";
}else{
    $sql = "select * from account order by accountId asc limit $offset, $rowsperpage";
}
$result = $mysqli->query($sql);


include(admin_tpl_path. 'account/manage_account_body.php');
