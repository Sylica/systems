<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

ini_set('display_errors', 1);
error_reporting(~E_DEPRECATED & ~E_NOTICE);

if (!defined('root_engine_path'))
    define('root_engine_path', '../engine/');

if (!defined('admin_page_path'))
    define('admin_page_path', './styles/pages/');

if (!defined('admin_tpl_path')) {
    define('admin_tpl_path', './styles/template/');
}

include(root_engine_path. 'init.php');
