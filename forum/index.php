<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

define('firestorm', true);
include('includes/loader.inc.php');

ob_start();
session_start();

$s = new forums();
$s->main_load();   

ob_end_flush();
