            <div id="footer">
                <div class="navs">
                    <div class="navs-bot">
                        <ul>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="copy">
                    Powered by Guild Management &copy; 2008 - <?php print date("Y"); ?> <br/>
                    <?php print $website->title; ?> &copy; <?php print date("Y"); ?> &bull; All rights reserved!
                </div>
            </div>
        </div>
    </div>
</body>
</html>
