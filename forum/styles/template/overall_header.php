<?php global $website, $news; ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php print $website->title; ?></title>
        <link rel="stylesheet" href="<?php print $website->forum_theme; ?>" />
        <script type="text/javascript" src="../styles/js/dropdown.js"></script>
    </head>
<body>
    <div id="wrapper">
        <div id="header">
            <h1><?php print $website->title; ?></h1>
        </div>
        <?php include('../styles/template/navbar_header.php'); ?> 
        <div class="clear"></div>

        <div id="content">
