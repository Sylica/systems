delete from system_groups where groupId between 1 and 3;
insert into system_groups (groupId, group_name, group_color) values (1, 'Member', '#5499C7');
insert into system_groups (groupId, group_name, group_color) values (2, 'Community Manager', '#f1c30f');
insert into system_groups (groupId, group_name, group_color) values (3, 'Administrator', '#ff0000');
