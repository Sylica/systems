/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `account` (
  `accountId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Account Identifier',
  `username` varchar(15) DEFAULT '',
  `sha_pass_hash` varchar(40) DEFAULT '',
  `email_address` varchar(50) DEFAULT '',
  `remote_address` varchar(50) DEFAULT '127.0.0.1',
  `permission` int(11) DEFAULT '1',
  `joined_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `online` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Account';

CREATE TABLE IF NOT EXISTS `account_data` (
  `accountId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_custom_title` varchar(15) DEFAULT NULL,
  `user_signature` longtext,
  `user_location` varchar(50) DEFAULT NULL,
  `user_avatar` text,
  `user_timezone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`accountId`),
  CONSTRAINT `FK_account_data_account` FOREIGN KEY (`accountId`) REFERENCES `account` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Account Data';

CREATE TABLE IF NOT EXISTS `account_settings` (
  `accountId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `avatar_height` int(11) DEFAULT NULL,
  `avatar_width` int(11) DEFAULT NULL,
  `forum_posts_per_page` int(11) NOT NULL DEFAULT '25',
  PRIMARY KEY (`accountId`),
  CONSTRAINT `FK_account_settings_account` FOREIGN KEY (`accountId`) REFERENCES `account` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Account Settings';

CREATE TABLE IF NOT EXISTS `community_application` (
  `accountId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_applied_as` int(11) DEFAULT NULL,
  `application_referred` varchar(50) DEFAULT NULL,
  `application_about` longtext,
  `application_experience` longtext,
  `application_interested` longtext,
  PRIMARY KEY (`accountId`),
  CONSTRAINT `FK_community_application_account` FOREIGN KEY (`accountId`) REFERENCES `account` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Community Application';

CREATE TABLE IF NOT EXISTS `community_news` (
  `newsId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `news_header` varchar(50) DEFAULT NULL,
  `news_message` longtext,
  `news_date` date DEFAULT NULL,
  `news_author` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`newsId`),
  KEY `FK_community_news_account` (`news_author`),
  CONSTRAINT `FK_community_news_account` FOREIGN KEY (`news_author`) REFERENCES `account` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Community News';

CREATE TABLE IF NOT EXISTS `system_countries` (
  `guid` int(10) NOT NULL AUTO_INCREMENT,
  `zone_id` char(11) NOT NULL,
  `timezone_strings` text,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='System Countries';

CREATE TABLE IF NOT EXISTS `system_groups` (
  `groupId` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` char(50) DEFAULT NULL,
  `group_color` char(50) DEFAULT NULL,
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='System Groups';

CREATE TABLE IF NOT EXISTS `system_logs` (
  `guid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `accountId` int(11) unsigned DEFAULT NULL,
  `logged_action` varchar(50) DEFAULT NULL,
  `remote_host` varchar(50) DEFAULT NULL,
  `logged_date` datetime DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `FK_system_logs_account` (`accountId`),
  CONSTRAINT `FK_system_logs_account` FOREIGN KEY (`accountId`) REFERENCES `account` (`accountId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Website Logging';

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
