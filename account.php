<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

define('firestorm', true);
include('engine/init.php');

ob_start();
session_start();

$s = new website();
$s->account_load();   

ob_end_flush();
