<?php

if (!defined('firestorm'))
    exit();

class cleaner {
    function filter($input) {
        $input = trim($input);
        $input = strip_tags($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);

        return $input;
    }
}

class bbcode {
    function bbcode_to_html($text) {
        $text = nl2br(htmlentities($text, ENT_QUOTES, 'UTF-8'));
        
        $in = array(
            '#\[b\](.*)\[/b\]#Usi',
            '#\[i\](.*)\[/i\]#Usi',
            '#\[u\](.*)\[/u\]#Usi',
            '#\[s\](.*)\[/s\]#Usi',
            '#\[img=w,(.*)\](.*)\[/img\]#Usi',
            '#\[imgleft=w,(.*)\](.*)\[/imgleft\]#Usi',
            '#\[url\]((ht|f)tps?\:\/\/(.*))\[/url\]#Usi',
            '#\[url=((ht|f)tps?\:\/\/(.*))\](.*)\[/url\]#Usi',
            '#\[color=(.*)\](.*)\[/color\]#Usi',
            '#\[left\](.*)\[/left\]#Usi',
            '#\[center\](.*)\[/center\]#Usi',
            '#\[right\](.*)\[/right\]#Usi'
        );

        $out = array(
            '<strong>$1</strong>',
            '<em>$1</em>',
            '<span style="text-decoration: underline;">$1</span>',
            '<span style="text-decoration: line-through;">$1</span>',
            '<img src="$2" width="$1" />',
            '<img src="$2" width="$1" style="float: left; padding-right: 15px;" />',
            '<a href="$1">$1</a>',
            '<a href="$1">$4</a>',
            '<font color="$1">$2</font>',
            '<div style="text-align: left;">$1</div>',
            '<div style="text-align: center;">$1</div>',
            '<div style="text-align: right;">$1</div>'
        );

        $count = count($in)-1;

        for ($i = 0; $i <= $count; $i++) {
            $text = preg_replace($in[$i], $out[$i], $text);
        }

        return $text;
    }

    function bbcode_signature($text) {
        $text = nl2br(htmlentities($text, ENT_QUOTES, 'UTF-8'));
        
        $in = array(
            '#\[b\](.*)\[/b\]#Usi',
            '#\[i\](.*)\[/i\]#Usi',
            '#\[u\](.*)\[/u\]#Usi',
            '#\[s\](.*)\[/s\]#Usi',
            '#\[img=w,(.*)\](.*)\[/img\]#Usi',
            '#\[imgleft=w,(.*)\](.*)\[/imgleft\]#Usi',
            '#\[url\]((ht|f)tps?\:\/\/(.*))\[/url\]#Usi',
            '#\[url=((ht|f)tps?\:\/\/(.*))\](.*)\[/url\]#Usi',
            '#\[color=(.*)\](.*)\[/color\]#Usi',
            '#\[left\](.*)\[/left\]#Usi',
            '#\[center\](.*)\[/center\]#Usi',
            '#\[right\](.*)\[/right\]#Usi'
        );

        $out = array(
            '<strong>$1</strong>',
            '<em>$1</em>',
            '<span style="text-decoration: underline;">$1</span>',
            '<span style="text-decoration: line-through;">$1</span>',
            '<img src="$2" width="$1" />',
            '<img src="$2" width="$1" style="float: left; padding-right: 15px;" />',
            '<a href="$1">$1</a>',
            '<a href="$1">$4</a>',
            '<font color="$1">$2</font>',
            '<div style="text-align: left;">$1</div>',
            '<div style="text-align: center;">$1</div>',
            '<div style="text-align: right;">$1</div>'
        );

        $count = count($in)-1;

        for ($i = 0; $i <= $count; $i++) {
            $text = preg_replace($in[$i], $out[$i], $text);
        }

        return $text;
    }
}
