<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

class website {
    function main_load() {
        include(root_engine_path. 'includes/connect.inc.php');

        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/'. $page .'.php')) ? '404' : $page;

        include(root_tpl_path. 'overall_header.php');
        include(page_path. $page .'.php');
        include(root_tpl_path. 'overall_footer.php');
    }

    function account_load() {
        include(root_engine_path. 'includes/connect.inc.php');
        
        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/account/'. $page .'.php')) ? '404' : $page;

        if (!isset($_SESSION['accId'])) {
            header("Location: index.php?action=login");
        }

        include(root_tpl_path. 'overall_header.php');
        include(page_path.'account/'. $page .'.php');
        include(root_tpl_path. 'overall_footer.php');
    }

    function member_load() {
        include(root_engine_path. 'includes/connect.inc.php');

        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/member/'. $page .'.php')) ? '404' : $page;

        if (!isset($_SESSION['accId'])) {
            header("Location: index.php?action=login");
        }

        include(root_tpl_path. 'overall_header.php');
        include(page_path.'member/'. $page .'.php');
        include(root_tpl_path. 'overall_footer.php');
    }
}

class forums {
    function main_load() {
        include(root_engine_path. 'includes/connect.inc.php');

        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/'. $page .'.php')) ? '404' : $page;

        include(forum_tpl_path. 'overall_header.php');
        include(forum_page_path. $page .'.php');
        include(forum_tpl_path. 'overall_footer.php');       
    }
}

class webadmin {
    function main_load() {
        include(root_engine_path. 'includes/connect.inc.php');

        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/'. $page .'.php')) ? '404' : $page;

        if (!isset($_SESSION['accId'])) {
            header("Location: /index.php?action=login");
        }

        include(admin_tpl_path. 'overall_header.php');
        include(admin_page_path. $page .'.php');
        include(admin_tpl_path. 'overall_footer.php');
    }

    function account_load() {
        include(root_engine_path. 'includes/connect.inc.php');

        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/account/'. $page .'.php')) ? '404' : $page;

        if (!isset($_SESSION['accId'])) { 
            header("Location: /index.php?action=login");
        }

        include(admin_tpl_path. 'overall_header.php');
        include(admin_page_path. 'account/'. $page .'.php');
        include(admin_tpl_path. 'overall_footer.php');
    }

    function news_load() {
        include(root_engine_path. 'includes/connect.inc.php');

        $page = (isset($_GET['action'])) ? $_GET['action'] : 'index';
        $page = (!file_exists('styles/pages/news/'. $page .'.php')) ? '404' : $page;

        if (!isset($_SESSION['accId'])) {
            header("Location: /index.php?action=login");
        }

        include(admin_tpl_path. 'overall_header.php');
        include(admin_page_path. 'news/'. $page .'.php');
        include(admin_tpl_path. 'overall_footer.php');
    }
}
