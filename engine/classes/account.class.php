<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

class account {
    function status($status) {
        switch ($status) {
            case 0: $status = 'Offline'; break;
            case 1: $status = 'Online'; break;
        }

        return $status;
    }
}
