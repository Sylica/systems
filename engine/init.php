<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

ini_set('display_errors', 1);
error_reporting(~E_DEPRECATED & ~E_NOTICE);

if (!defined('root_engine_path'))
    define('root_engine_path', './engine/');

if (!defined('root_tpl_path'))
    define('root_tpl_path', './styles/template/');

if (!defined('page_path'))
    define('page_path', './styles/pages/');

foreach (glob(root_engine_path. 'classes/*.php') as $class) {
    include $class;
}

foreach (glob(root_engine_path. 'includes/*.php') as $inc) {
    include $inc;
}
