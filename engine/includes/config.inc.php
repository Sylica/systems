<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$website = (object) array(
    'url' => '',
    'title' => 'Eternity Community Site',
    'main_theme' => 'styles/default/stylesheet.css',
    'forum_theme' => '../styles/default/stylesheet.css',
);

$news = (object) array(
    'display_limit' => 15,
    'display_range' => 5,
);
