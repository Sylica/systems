<?php
/***
 * @project name: Firestorm aka (Guild Management)
 * @project copyright: 2016 - 2017
 * @project author: Meltie
 */

if (!defined('firestorm'))
    exit();

$db_host = 'localhost';
$db_user = 'root';
$db_pass = '';
$db_name = '';

$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_name);
if ($mysqli->connect_errno) {
    die('Connection failed: '. $mysqli->connect_error);
}

return $mysqli;
